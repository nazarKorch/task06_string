package regex;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Word {

  private Book book = new Book();

  private Pattern p, p2;
  private Matcher m, m2;

  public void showUniqueWordInFirstSentence() {
    String s = book.readFromFile();
    String[] firstSentence = null;
    ArrayList<String> sentences = new ArrayList<>();
    int start = 0;
    p = Pattern.compile("[.](\\W|$)");
    m = p.matcher(s);

    if (m.find()) {
      firstSentence = s.substring(start, m.start()).split("\\s+");
      start = m.end();
    }

    while (m.find()) {
      sentences.add(s.substring(start, m.start()));
      start = m.end();
    }

    if (firstSentence != null) {
      for (int i = 0; i < firstSentence.length; i++) {
        for (String sentence : sentences) {
          String[] str = sentence.split("\\s+");
          for (int j = 0; j < str.length; j++) {
            if (!firstSentence[i].equals("null")) {
              if (str[j].equals(firstSentence[i])) {
                firstSentence[i] = "null";
              }
            }
          }
        }
      }
    }

    System.out.println("*Unique words: ");
    for (int i = 0; i < firstSentence.length; i++) {
      if (!firstSentence[i].equals("null")) {
        System.out.println(firstSentence[i]);
      }
    }

  }



  public void swapFirstVowelWithLongest() {

    String s = book.readFromFile();
    String[] firstSentence = null;
    ArrayList<String> sentences = new ArrayList<>();
    int start = 0;
    p = Pattern.compile("[.](\\W|$)");
    p2 = Pattern.compile("(\\b|^)[aeiou]\\w+");
    m = p.matcher(s);

    while (m.find()) {
      sentences.add(s.substring(start, m.start()));
      start = m.end();
    }

    for (String sentence : sentences) {
      int longestIndex = 0;
      String longest = "";
      String[] words = sentence.split("\\s+");

      for (int i = 0; i < words.length; i++) {
        if (words[i].length() > longest.length()) {
          longest = words[i];
          longestIndex = i;
        }
      }
      for (int j = 0; j < words.length; j++) {
        m2 = p2.matcher(words[j]);
        if (m2.find()) {
          String t = words[j];
          words[j] = longest;
          words[longestIndex] = t;
          break;
        }
      }
      for (int k = 0; k < words.length; k++) {
        System.out.print(" " + words[k]);
      }
      System.out.println(".");
    }
  }


  public void sortWordsAlphabetical() {

    String firstLetter = "";
    String s = book.readFromFile();
    ArrayList<String> words = new ArrayList<>();
    int start = 0;
    p = Pattern.compile("(\\b|^)[\\w]");
    p2 = Pattern.compile("\\s+");
    m2 = p2.matcher(s);

    while (m2.find()) {
      words.add(s.substring(start, m2.start()));
      start = m2.end();
    }
    words.add(s.substring(start, s.length()));

    Collections.sort(words);

    System.out.println(words);
    for (int i = 0; i < words.size(); i++) {
      m = p.matcher(words.get(i));
      if (m.find()) {
        if (firstLetter.equals("")) {
          firstLetter = m.group();
        } else {
          if (!firstLetter.equals(m.group())) {
            words.set(i, "  " + words.get(i));
            firstLetter = m.group();
          }
        }
      }
      System.out.println(words.get(i));
    }
  }


  public void sortWordsByPercentegeVowel() {
    String s = book.readFromFile();
    ArrayList<String> words = new ArrayList<>();
    int start = 0;

    p = Pattern.compile("[aeiou]");
    p2 = Pattern.compile("\\s+");
    m2 = p2.matcher(s);

    while (m2.find()) {
      words.add(s.substring(start, m2.start()));
      start = m2.end();
    }
    words.add(s.substring(start, s.length()));

    Comparator<String> comparator = (str1, str2) -> {

      int size1 = str1.length();
      int size2 = str2.length();
      int vowels = 0;
      int vowels2 = 0;
      Double res1 = 0.0;
      Double res2 = 0.0;
      m = p.matcher(str1);

      while (m.find()) {
        vowels++;
      }

      m = p.matcher(str2);
      while (m.find()) {
        vowels2++;
      }
      try {
        res1 = (double) vowels / size1;
        res2 = (double) vowels2 / size2;

      } catch (Exception e) {
        System.out.println(e.getMessage());

      }
      return res2.compareTo(res1);
    };

    Collections.sort(words, comparator);

    for (String word : words) {
      System.out.println(word);
    }
  }


  public void sortWordsByNumberOfSomeLetter(char c) {
    String s = book.readFromFile();
    ArrayList<String> words = new ArrayList<>();
    int start = 0;
    char someLetter = c;

    p = Pattern.compile("[" + someLetter + "]");
    p2 = Pattern.compile("\\s+");
    m2 = p2.matcher(s);

    while (m2.find()) {
      words.add(s.substring(start, m2.start()));
      start = m2.end();
    }
    words.add(s.substring(start, s.length()));

    Comparator<String> comparator = (str1, str2) -> {

      int size1 = str1.length();
      int size2 = str2.length();
      int numberOfLetters = 0;
      int numberOfLetters2 = 0;
      Double r1 = 0.0;
      Double r2 = 0.0;
      m = p.matcher(str1);

      while (m.find()) {
        numberOfLetters++;
      }

      m = p.matcher(str2);
      while (m.find()) {
        numberOfLetters2++;
      }

      if ((numberOfLetters2 - numberOfLetters) == 0) {
        return str1.compareTo(str2);
      }

      return numberOfLetters2 - numberOfLetters;
    };

    Collections.sort(words, comparator);

    for (String word : words) {
      System.out.println(word);
    }
  }




  public void findForEachWordOccurrenceNumber(List<String> list) {
    String s = book.readFromFile();
    Map<Integer, String> map = new TreeMap<>();

    int count = 0;

    for (int i = 0; i < list.size(); i++) {
      p = Pattern.compile(list.get(i));
      m = p.matcher(s);
      while (m.find()) {
        count++;
      }
      map.put(count, list.get(i));
      count = 0;
    }
    for(Integer i : map.keySet()){
      System.out.println(i + " - " + map.get(i));
    }


  }

}
