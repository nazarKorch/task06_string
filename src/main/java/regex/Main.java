package regex;


import java.io.FileNotFoundException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

public class Main {


  public static void main(String[] args) {

    Sentence sentence = new Sentence();
    Word word = new Word();
    System.out.println("*Number of sentences with equal words: "
        + sentence.countSentencesWithEqualWords());

    System.out.println("*Sentences sorted by number of words:");
    sentence.showSentencesSorted();

    word.showUniqueWordInFirstSentence();

    System.out.println("*Sentences with swapped first vowel started word and longest word: ");
    word.swapFirstVowelWithLongest();

    System.out.println("*Words sorted by alphabetical order:");
    word.sortWordsAlphabetical();

    System.out.println("*words sorted by percentage of vowels:");
    word.sortWordsByPercentegeVowel();

    char letter = 'f';
    System.out.println("*words sorted by occurrence number of letter " + letter);
    word.sortWordsByNumberOfSomeLetter(letter);

    List<String> list = Arrays.asList("java", "and");
    System.out.println("*find occurrence number of each word from list:");
    word.findForEachWordOccurrenceNumber(list);









  }


}
