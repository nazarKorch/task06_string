package regex;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Sentence {

  private Book book = new Book();

  private Pattern p, p2;
  private Matcher m, m2;

  public int countSentencesWithEqualWords() {

    ArrayList<String> words = new ArrayList<>();
    String s = book.readFromFile();

    int count = 0;
    int letter = 0;
    int start = 0;

    p = Pattern.compile("[.](\\W|$)");
    p2 = Pattern.compile("\\s+");
    m = p.matcher(s);
    m2 = p2.matcher(s);
    boolean find = true;

    while (m.find()) {
      while (find) {
        if (m2.find()) {
          if (m2.start() >= m.start()) {
            String word = s.substring(start, m.start());
            if (words.contains(word)) {
              count++;
            }
            start = m2.end();
            break;
          }
          String word = s.substring(start, m2.start());
          if (words.contains(word)) {
            count++;
          } else {
            words.add(word);
          }
          start = m2.end();
        } else {
          find = false;
          String word = s.substring(start, m.start());
          if (words.contains(word)) {
            count++;
          }
        }
      }

      if (count != 0) {
        letter++;
        count = 0;
      }
      words = new ArrayList<>();
    }
    return letter;
  }


  public void showSentencesSorted() {
    String s = book.readFromFile();
    ArrayList<String> sentences = new ArrayList<>();
    int start = 0;
    p = Pattern.compile("[.](\\W|$)");
    m = p.matcher(s);

    while (m.find()) {

      sentences.add(s.substring(start, m.start()));
      start = m.end();

    }

    Comparator<String> comparator = (str, str2) -> {
      String[] words = str.split("\\w");
      String[] words2 = str2.split("\\w");
      return (words.length) - (words2.length);
    };

    Collections.sort(sentences, comparator);

    for (String sentence : sentences) {
      System.out.println(sentence + ".");
    }

  }


}
