package I18n;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Scanner;

public class Menu {

  private Scanner scanner = new Scanner(System.in);
  private ResourceBundle resourceBundle;
  private Locale locale;

  private Map<String, String> menuMap;

  public Menu() {
    setMenuEnglish();
    setMenu();
    showMenu();


  }

  private void setMenu() {

    menuMap = new LinkedHashMap<>();
    menuMap.put("1", resourceBundle.getString("1"));
    menuMap.put("2", resourceBundle.getString("2"));
    menuMap.put("3", resourceBundle.getString("3"));
    menuMap.put("4", resourceBundle.getString("4"));
    menuMap.put("5", resourceBundle.getString("5"));
    menuMap.put("q", resourceBundle.getString("q"));

  }


  private void setMenuEnglish() {

    locale = new Locale("en");
    resourceBundle = ResourceBundle.getBundle("Menu", locale);

  }

  private void setMenuFrench() {
    locale = new Locale("fr");
    resourceBundle = ResourceBundle.getBundle("Menu", locale);
  }

  private void setMenuItalian() {
    locale = new Locale("it");
    resourceBundle = ResourceBundle.getBundle("Menu", locale);
  }

  private void showLanguage() {
    System.out.println("1 - French");
    System.out.println("2 - Italian");
    System.out.println("q - exit");
  }

  private void languageChange() {
    String input;
    try {
      input = scanner.nextLine();
      switch (input) {
        case "1":
          setMenuFrench();
          setMenu();
          showMenu();
          break;
        case "2":
          setMenuItalian();
          setMenu();
          showMenu();
          break;
        case "q":
          showMenu();
          break;
        default:
          languageChange();
          break;

      }
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }

  }

  private void showMenu() {

    String input;

    for (String key : menuMap.keySet()) {

      System.out.println(menuMap.get(key));
    }
    try {
      input = scanner.nextLine();
      while (!input.equals("q")) {
        switch (input) {
          case "1":
            showMenu();
            break;
          case "2":
            showMenu();
            break;
          case "3":
            showMenu();
            break;
          case "4":
            showMenu();
            break;
          case "5": {
            showLanguage();
            languageChange();
            break;
          }
          default:
            System.out.println("wrong number");
            showMenu();
            break;


        }
      }

    } catch (Exception e) {
      System.out.println(e.getMessage());
    }


  }

}
