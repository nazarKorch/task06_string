package StringUtils;


public class StringUtils {

  private String str = "";

  public StringUtils(Object... arg) {
    for (Object object : arg) {
      str = str.concat(String.valueOf(object));
    }
  }

  public StringUtils() {

  }

  public String append(Object object) {
    return str = str.concat(String.valueOf(object));
  }

  public String toString() {
    return str;
  }


}
